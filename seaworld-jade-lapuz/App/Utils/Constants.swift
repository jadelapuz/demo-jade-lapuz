//
//  Constants.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/3/20.
//  Copyright © 2020 Jade Lapuz. All rights reserved.
//

import Foundation

struct WebSocketConstants {
    static let CONNECTION_URL = "ws://sea-world.sibext.com/cable?access_token=d994fe8e40788ceb4282bb02bab9534fe805b9ca"
}

enum Commands
{
    case subscribe
    case unsubscribe
    case message
    
    func getValue() -> String
    {
        switch self {
        case .subscribe:
            return "subscribe"
        case .unsubscribe:
            return "unsubscribe"
        case .message:
            return "message"
        }
    }
}

enum Action
{
    case nextStep
    case reset
    
    func getValue() -> String
    {
        switch self {
       
        case .nextStep:
            return "next_step"
        case .reset:
            return "reset"
        }
    }
}
