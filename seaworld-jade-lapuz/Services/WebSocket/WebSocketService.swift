//
//  WebSocketService.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/3/20.
//  Copyright © 2020 Jade Lapuz. All rights reserved.
//

import Foundation
import Starscream

protocol WebSocketServiceProtocol {
    func connected(headers: [String: String])
    func disconnected(reason: String, code: UInt16)
    func didReceivedText(text: String)
    func didReceivedData(data: Data)
}
class WebSocketService
{
    public static let shared = WebSocketService()
    
    private var socket: WebSocket!
    
    var delegate: WebSocketServiceProtocol?
    
    private init()
    {
        
    }
    
    public func connect()
    {
        // sanitary check
        if let url = URL(string: WebSocketConstants.CONNECTION_URL)
        {
            var request = URLRequest(url: url)
                request.timeoutInterval = 10
                socket = WebSocket(request: request)
                socket.delegate = self
                socket.connect()
        } else
        {
            // add alert invalid url
        }
    }
    
    public func write(text: String?)
    {
        print("your request", text!)
        socket.write(string: text!)
    }
}

extension WebSocketService: WebSocketDelegate
{
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            print("websocket is connected: \(headers)")
            delegate?.connected(headers: headers)
        case .disconnected(let reason, let code):
            print("websocket is disconnected: \(reason) with code: \(code)")
            delegate?.disconnected(reason: reason, code: code)
        case .text(let string):
            delegate?.didReceivedText(text: string)
        case .binary(let data):
            delegate?.didReceivedData(data: data)
        case .ping(_):
            break
        case .pong(_):
            break
        case .viablityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            break
        case .error(let error):
            print(error)
            break
        }
    }
}
