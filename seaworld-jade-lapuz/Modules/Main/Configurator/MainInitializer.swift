//
//  MainMainInitializer.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/03/2020.
//  Copyright © 2020 seaworld. All rights reserved.
//

import UIKit

class MainModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var mainViewController: MainViewController!

    override func awakeFromNib() {

        let configurator = MainModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: mainViewController)
    }

}
