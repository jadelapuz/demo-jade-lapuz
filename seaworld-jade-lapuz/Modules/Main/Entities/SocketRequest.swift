//
//  SocketRequest.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/3/20.
//  Copyright © 2020 Jade Lapuz. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SocketRequest {
    var command: Commands!
    var identifier: Identifier!
    var data: Action?
    
    public func toJson() -> String?
    {
        var dict: [String: Any] =  ["command": command.getValue(),
                                    "identifier": "{\"channel\":\"\(identifier.channel!)\", \"id\":\"\(identifier.id!)\"}"]
        
        if let data = data
        {
            dict["data"] = dictionaryToJson(dict: ["action": data.getValue()], option: .withoutEscapingSlashes)
        }
        
        return dictionaryToJson(dict: dict, option: .withoutEscapingSlashes)
    }
    
    func dictionaryToJson(dict: [String: Any], option: JSONSerialization.WritingOptions) -> String?
    {
        let data = try! JSONSerialization.data(withJSONObject: dict, options: option)
        
        return String(data: data, encoding: .utf8)
    }
}

