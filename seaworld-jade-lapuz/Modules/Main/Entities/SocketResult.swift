//
//  RootClass.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on March 19, 2020

import Foundation
import SwiftyJSON

class SocketResult : NSObject, NSCoding{

    var identifier : String!
    var message : Message!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        identifier = json["identifier"].stringValue
        let messageJson = json["message"]
        if !messageJson.isEmpty{
            message = Message(fromJson: messageJson)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if identifier != nil{
            dictionary["identifier"] = identifier
        }
        if message != nil{
            dictionary["message"] = message.toDictionary()
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        identifier = aDecoder.decodeObject(forKey: "identifier") as? String
        message = aDecoder.decodeObject(forKey: "message") as? Message
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if identifier != nil{
            aCoder.encode(identifier, forKey: "identifier")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }

    }

}
