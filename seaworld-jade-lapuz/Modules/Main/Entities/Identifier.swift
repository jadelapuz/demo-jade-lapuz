//
//  Identifier.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/3/20.
//  Copyright © 2020 Jade Lapuz. All rights reserved.
//

import Foundation

struct Identifier {
    var channel: String!
    var id: String!
    
    init() {
        channel = "WorldChannel"
        id = 1.description
    }
    
    init(id: String) {
        channel = "WorldChannel"
        self.id = id
    }
}
