//
//  MainMainViewInput.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/03/2020.
//  Copyright © 2020 seaworld. All rights reserved.
//

protocol MainViewInput: class {

    /**
        @author Jade Lapuz
        Setup initial state of the view
    */

    func setupInitialState()
    func socketConnected()
    func didReceiveResponse(socketResult: SocketResult)
    func showError()
}
