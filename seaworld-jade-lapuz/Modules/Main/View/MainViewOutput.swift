//
//  MainMainViewOutput.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/03/2020.
//  Copyright © 2020 seaworld. All rights reserved.
//

protocol MainViewOutput {

    /**
        @author Jade Lapuz
        Notify presenter that view is ready
    */

    func viewIsReady()
    func write(socketRequest: SocketRequest)
    func connect()
    func disconnect()
}
