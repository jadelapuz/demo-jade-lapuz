//
//  MainMainViewController.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/03/2020.
//  Copyright © 2020 seaworld. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, MainViewInput {

    var output: MainViewOutput!

    private var dataSource: SocketResult?
    private var socketRequest = SocketRequest()
    private var activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var worldTextField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: MainViewInput
    func setupInitialState() {
        setUpViews()
        output.connect()
    }
    
    func setUpViews()
    {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: String(describing: CreatureCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CreatureCollectionViewCell.self))
    }
    
    func socketConnected() {
        socketRequest.command = .subscribe
        // default world is 1
        socketRequest.identifier = Identifier()
        output.write(socketRequest: socketRequest)
        self.showActivityIndicatory()
    }
    
    func didReceiveResponse(socketResult: SocketResult) {
        self.dataSource = socketResult
        self.collectionView.reloadData()
        self.buttonStackView.isHidden = false
        hideActivityIndicatory()
    }
    
    @IBAction func changeWorld(_ sender: UIButton) {
        if worldTextField.text!.isEmpty
        {
            let alert = UIAlertController(title: "Failed", message: "Please enter world ID", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
               
            }))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        socketRequest.command = .unsubscribe
        socketRequest.identifier = Identifier()
        socketRequest.data = nil
        output.write(socketRequest: socketRequest)
        
        self.socketRequest.command = .subscribe
        let id = Identifier(id: worldTextField.text ?? 1.description)
        self.socketRequest.identifier = id
        self.output.write(socketRequest: self.socketRequest)
        self.showActivityIndicatory()
    }
    
    @IBAction func restart(_ sender: UIButton) {
        socketRequest.data = .reset
        socketRequest.command = .message
        socketRequest.identifier = Identifier()
        output.write(socketRequest: socketRequest)
        //clear the collectionview right away. It will populate again if there is a reponse from the action: reset call
        dataSource = nil
        self.collectionView.reloadData()
    }
    
    @IBAction func nextTurn(_ sender: UIButton) {
        socketRequest.data = .nextStep
        socketRequest.command = .message
        socketRequest.identifier = Identifier()
        output.write(socketRequest: socketRequest)
        showActivityIndicatory()
    }
    
    func showActivityIndicatory() {
        activityView.center = self.view.center
        self.view.addSubview(activityView)
        if !activityView.isAnimating
        {
            activityView.startAnimating()
        }
    }
    
    func hideActivityIndicatory() {
        activityView.hidesWhenStopped = true
        activityView.stopAnimating()
    }
    
    func showError() {
        let alert = UIAlertController(title: "Failed", message: "Invalid World ID", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.hideActivityIndicatory()
            self.dataSource = nil
            self.collectionView.reloadData()
            self.buttonStackView.isHidden = true
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}


extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.message.map[section].count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource?.message.map.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CreatureCollectionViewCell.self), for: indexPath) as! CreatureCollectionViewCell
        
        if let source = dataSource
        {
            if let imageNamed = source.message.map[indexPath.section][indexPath.row].string
            {
                cell.cellImage.image = UIImage(named: imageNamed)
            } else
            {
                cell.cellImage.image = nil
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int(collectionView.bounds.width / 5)
        let height = collectionView.bounds.height / 5
        
        return CGSize(width: width, height: Int(height))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
