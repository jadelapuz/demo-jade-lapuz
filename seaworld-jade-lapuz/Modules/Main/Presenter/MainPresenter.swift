//
//  MainMainPresenter.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/03/2020.
//  Copyright © 2020 seaworld. All rights reserved.
//

class MainPresenter: MainModuleInput, MainViewOutput, MainInteractorOutput {
  
    weak var view: MainViewInput!
    var interactor: MainInteractorInput!
    var router: MainRouterInput!
    
    //MARK: View
    func viewIsReady() {
        view.setupInitialState()
    }

    func connect() {
        interactor.connect()
    }
    
    func disconnect() {
        
    }
    
    func write(socketRequest: SocketRequest) {
        interactor.write(socketRequest: socketRequest)
    }
    
    //MARK: interactor
    func socketConnected() {
        view.socketConnected()
    }
    
    func didReceiveResponse(socketResult: SocketResult) {
        view.didReceiveResponse(socketResult: socketResult)
    }
    
    func showError() {
        view.showError()
    }
}
