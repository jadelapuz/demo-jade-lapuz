//
//  CreatureCollectionViewCell.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/3/20.
//  Copyright © 2020 Jade Lapuz. All rights reserved.
//

import UIKit

class CreatureCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
