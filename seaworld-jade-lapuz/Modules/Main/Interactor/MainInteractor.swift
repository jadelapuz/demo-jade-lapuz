//
//  MainMainInteractor.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/03/2020.
//  Copyright © 2020 seaworld. All rights reserved.
//
import Foundation
import SwiftyJSON

class MainInteractor: MainInteractorInput {
   
    weak var output: MainInteractorOutput!
    
    private var socketService = WebSocketService.shared
    
    init() {
       
    }
    
    func connect() {
        socketService.delegate = self
        socketService.connect()
    }
    
    func write(socketRequest: SocketRequest) {
        socketService.write(text: socketRequest.toJson())
    }
}


extension MainInteractor: WebSocketServiceProtocol
{
    func connected(headers: [String : String]) {
        output.socketConnected()
    }
    
    func disconnected(reason: String, code: UInt16) {
    }
    
    func didReceivedText(text: String) {
        print(text)
        let json = JSON.init(parseJSON:text)
        if json["type"].stringValue == "reject_subscription" {
            output.showError()
        } else if text.contains("map")
        {
            let result = SocketResult(fromJson: json)
            output.didReceiveResponse(socketResult: result)
        }
    }
    
    func didReceivedData(data: Data) {
        
    }
    
    
}
