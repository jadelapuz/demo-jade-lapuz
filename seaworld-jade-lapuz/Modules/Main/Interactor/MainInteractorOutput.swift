//
//  MainMainInteractorOutput.swift
//  seaworld-jade-lapuz
//
//  Created by Jade Lapuz on 19/03/2020.
//  Copyright © 2020 seaworld. All rights reserved.
//

import Foundation

protocol MainInteractorOutput: class {

    func socketConnected()
    func didReceiveResponse(socketResult: SocketResult)
    func showError()
}
